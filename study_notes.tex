\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber]{biblatex}
\addbibresource{references.bib}

\usepackage{enumitem}
\usepackage{xcolor}
\usepackage{amsthm}
\usepackage{amsmath}

\newtheorem*{definition}{Definition}

\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={red!50!black},
  citecolor={blue!50!black},
  urlcolor={blue!80!black}
}

\title{CSC469 Study Notes}
\author{Jad Elkhaleq Ghalayini}
\date{December 2019}

\begin{document}

\maketitle

\tableofcontents

\section{Parallel Job Scheduling}

\begin{definition}[Job]
A \textbf{job} is a collection of processes and/or threads which co-operate to provide a service.
Note that the processes/threads in a job are not independent.
\end{definition}
The way in which the components of a job are scheduled can have a major effect on performance. Hence, we want the scheduler to be aware of dependencies between threads and processes. We will look at multiple strategies for scheduling jobs:
\begin{itemize}

  \item \textbf{Space sharing} in which each job has dedicated processors

  \item \textbf{Time sharing} in which multiple jobs share the same processor

\end{itemize}
As the threads in a job are not independent, they may interact with each other in a variety of ways. These include
\begin{itemize}

  \item \textbf{Synchronizing over shared data} (e.g. mutexes). This means that if the lock holder is descheduled, other threads in the job may not get far.

  \item \textbf{Cause/effect relationships} (e.g. the producer/consumer problem). For example, in a producer/consumer situation, the consumer may be waiting for data from the producer while the producer is descheduled.

  \item \textbf{Synchronizing phases of execution (barriers)}, which can mean the entire job proceeds at the pace of the slowest thread.

\end{itemize}
There are many ways in which the scheduler can be made aware of the dependencies between threads:
\begin{itemize}

  \item Threads known to be related can all be scheduled at the same time. In space-sharing, all threads are from the same job, whereas in time-sharing, threads that should be scheduled together can be grouped.


  \item If we know when threads hold spinlocks, we can avoid descheduling the lock holder, extending the timeslice (but not indefinitely).

  \item We can try to infer general dependencies, e.g. producer/consumer relationships from the existence of a pipe.

\end{itemize}

\section{Superpages}

\subsection{Paging Review}

\subsubsection{64-bit address space}

Most modern 64-bit architectures do not actually support a full 64-bit virtual address space, instead having a 64-bit architecture with a 48-bit address space to save transistors. Since the instruction set architecture itself is 64-bit, it can be extended to support a larger virtual address space without breaking backwards compatibility. In x86-64, 48-bit addresses are composed of a 9-bit index for four page table levels followed by a 12-bit offset.

\subsubsection{The Translation Lookaside Buffer (TLB)}

The TLB translates virtual addresses into page table entries (\textit{not} physical addresses!). This can be done in a single machine cycle. TLBs are implemented in hardware, and are mostly fully associative caches. With a page table entry plus an offset, we can directly calculate virtual addresses.

TLBs exploit locality, namely that processes use only a handful of pages at a time, and hence, only those pages need to be ``mapped." A 16-64 entry TLB, for example, can map 16-64 pages, and hence 64-256K of memory assuming 4KB pages.

Hit rates are very important: typically >99\% of translations should be hits, as misses will trigger the need to do a costly lookup. How well does it work, though?

\textbf{TLB coverage} is how much memory can be accessed without incurring misses. For example, if we have 128 entries and 4KB pages, we cover 512 KB of memory. Typical TLB coverage is 1MB, which is not a lot at all. In fact, TLB coverage as a percentage of main memory has decreased by a factor of 100 in the last 15 years, as memory has grown and TLBs have not.

How can we increase TLB coverage? We can:
\begin{itemize}

  \item Increase TLB size, but then access time goes up

  \item Increase page size, but this increases fragmentation, memory pressure and IO

  \item Compromise: use superpages!

\end{itemize}

\subsection{Superpages}

\textbf{Superpages} are larger than usual pages. We allow both large and small pages, each with a power of 2 size. Each superpage requires a single TLB entry: superpages are contiguous and both virtually and physically aligned. They have uniform attributes with normal pages: protection, valid, ref, dirty.

As a benefit, these increase TLB coverage without increasing TLB size, while simultaneously keeping internal fragmentation and disk traffic low. That said, there are some hardware constraints to keep in mind:

\begin{itemize}

  \item Superpage sizes must be power of two multiples of the base page size

  \item Superpages must be aligned in both virtual and physical memory, i.e. a 4 MB superpage must have a starting address which is a multiple of 4 MB.

  \item TLB entries for superpages have only a single reference bit, dirty bit and set of protection bits, and must also store the superpage size.

  \item The desired superpage sizes (and, of course, superpages in general) must be supported by the MMU of the target processor.

\end{itemize}
The main issues to deal with in implementing superpages are
\begin{itemize}

  \item Allocation: what page frame to choose on page fault

  \item Promotion: combining suitable base pages into superpages

  \item Demotion: breaking a superpage up into smaller superpages or base pages

  \item Fragmentation: we need contiguous physical pages to create superpages

\end{itemize}

\subsubsection{Allocation and Promotion}

When bringing a page into main memory, we can either:
\begin{itemize}

  \item Put it anywhere in RAM, but then we will need to relocate it to a suitable place when we merge it into a superpage. This is \textbf{relocation-based allocation}.

  Implementations of relocation based allocation:
  \begin{itemize}

    \item Research (e.g. Romer et. al. ``Reducing TLB and memory overhead using online superpage promotion"): move pages at \textbf{promotion time}. Must recover copying costs.

    \item Commercial OS: superpages allocated at page fault time, with user-specified size (not transparent).

    \begin{itemize}

      \item IRIX: can select different page sizes for suitably aligned ranges of virtual address space. OS maintains free list of pages of each size, which a \textbf{coalescing daemon} periodically tries to refresh. Larger pages can be demoted under memory pressure.

      \item HP-UX: can select different page sizes for text and data segment only. The hint is associated with the binary, i.e. cannot be selected at runtime.

    \end{itemize}

  \end{itemize}

  \item Put it in a location that would let us ``grow" a superpage around it. Note this means we must pick a maximum size for the superpage. This is \textbf{reservation-based allocation}.

  Implementations of reservation based allocation:
  \begin{itemize}

    \item Research: Talluri \& Hill ``Surpassing the TLB performance of superpageswith less operating system support": uses only one superpage size and designed to work with a proposed \textbf{partial sub-block TLB} (a TLB entry with only one PPN but potentially multiple valid bits)

  \end{itemize}

\end{itemize}

\subsubsection{Navarro et al.'s Design}

Key observation: once an application touches the first page of a memory object then it is likely that it will quickly touch every other page of that object (e.g. array initialization). We use an optimistic policy: we have no \textit{a priori} knowledge other base pages of the superpage will quickly be accessed, and superpages are as large as possible (up to the size of the memory object) and allocated as soon as possible (even on the first page fault).

\subsubsection{Demotion}

\subsubsection{Fragmentation Controls}

\section{Coloring and NUMA}

Recall that 3 policies characterize a virtual memory management scheme:
\begin{itemize}

  \item \textbf{Fetch policy}: when to fetch a page?

  \item \textbf{\textit{Placement policy}}: \textit{where} to put the page? We have to ask whether some physical pages are preferable to others.

  \item \textbf{Replacement policy}: what pages to evict to make room.

\end{itemize}
Address translation allows us to map any virtual address to any physical page. However, choosing phsical pages carefully can be better than random placement. We'll look at two reasons why: cache conflicts and NUMA (Non Uniform Memory Access) multiprocessors.

\subsection{Cache Access}

In modern architectures, data is loaded into the cache in blocks called \textbf{lines}. Line sizes of 32 to 128 bytes are typical. Cache lines may not be able to map to every area in memory, however. There are three common situations:
\begin{itemize}

  \item In a \textbf{direct-mapped} cache, each block can be stored in exactly one location in the cache. The mapping from block address to cache address is then simply \verb|block address % (#blocks in cache)|.

  \item If any block can be stored in any cache line, the cache is called \textbf{fully associative}.

  \item If each block can be stored in a restricted set of locations in the cache, the chache is called \textbf{set associative}. We map a block address to a set using \verb|block address % (#sets)|, and then place the block anywhere in the set. If there are \(N\) locations in the set, we call the cache \textbf{\(N\)-way set associative}.

\end{itemize}
How cache conflicts should be dealt with depends on which address is used to determine cache lines.
\begin{itemize}

  \item If \textbf{virtual addresses} are used, then addresses do not need to be translated to check the cache, and the application programmer can reason about conflicts. In this case, however, the cache needs to be flushed on context switches.

  \item If \textbf{physical addresses} are used, data can stay in the cache across context switches, but addresses must be translated before checking the cache and conflicts depend on what physical page is allocated.

\end{itemize}

\subsubsection{Coloring}
Assume now we use physical addresses to determine cache lines. The OS can then try to select physical pages on allocation to reduce cache conflicts. One idea is to assign a \textbf{color} to each page such that pages with different colors do not conflict in the cache.
\begin{itemize}

  \item We assume all pages with the same color map to the same lines in the cache

  \item We set
  \[\text{no. colors} = \frac{\text{cache size}}{\text{page size} \cdot \text{associativity}}\]

  \item A page's color is the page number modulo the number of colors.

\end{itemize}
There are two main page allocation strategies we can then use: page coloring and bin hopping. Both these strategies use colors to try to avoid cache conflicts.

\subsubsection{Page Coloring}

We assign colors to both virtual and physical pages. On a page fault, we allocate a physical page with the same color as the virtual page. This takes advantage of \textit{spatial locality}. Each color gets its own free list. This strategy was implemented bh SGI Irix, Solaris and NT.

\subsubsection{Bin Hopping}

We assign colors \textit{only} to physical pages. Every page fault, we allocate a page with a new color, cycling through our set of colors. This takes advantage of \textit{temporal locality}. Again, each color gets its own free list. This strategy was implemented by Digital Unix.

\subsection{Non Uniform Memory Access (NUMA)}

In a NUMA multiprocessor architecture, each processor or small set of processors has a bank of local memory, but can also access remote memory, with local memory accesses being slower than remote memory access. Hence, the speed of memory access is \textit{non-uniform}.

\subsection{NUMA Page Placement}

The goal of page placement in a NUMA architecture is to allocate ``local" memory as much as possible. The issue is that local at allocation time may not be local at access time, i.e. we might want to migrate pages. One solution is to keep per-memory-bank free lists, possibly in addition to per-color free lists.

SGI Irix made the NUMA placement policy user-selectable, with users being able to choose a round-robin, random or first touch page placement policy as well as whether page migration was enabled. Linux has the \verb|numactl| command line tool, built using the \verb|get_memory_policy|, \verb|set_memory_policy| and \verb|mbind| APIs. The default is to allocate from local memory, and the scheduler tries to avoid migrating threads to other CPUs.

\section{Distributed Shared Memory}

\subsection{Distributed Systes}

A \textbf{distributed system} is composed of\textit{multiple} computers.
\begin{itemize}
  \item These may be \textbf{heterogeneous}, i.e. having different architectures, or \textbf{homogeneous}.
  \item They may be controlled by a single organization or may be spread out across multiple organizations and networks.
  \item \textbf{No} physical shared memory \textbf{or} shared clock.
\end{itemize}
These computers must be connected by a \textbf{communication network}, which is typically a general-purpose network not dedicated to supporting the distributed system. The computers using distributed shared memory cooperate to share resources and services, with application processing occuring on more than one machine.

\subsection{Distributed Interprocess Communication (IPC)}

There are two main methods of implementing distributed IPC. The first is to use \textbf{message passing primitives}, e.g. Unix sockets. These are a good match for the underlying structure, but the programmer has to deal with passing data. An alternative is using \textbf{remote procedure calls} (RPC). This provides a familiar programming model, and complex communication details are handled by RPC. However, passing complex datatypes is hard, and the model is synchronous, which is not a good fit for parallel programming.

\subsection{Shared Memory}

\subsubsection{Local Shared Memory}

Local shared memory can be implemented on uniprocessor, symmetric multiprocessing (SMP) or NUMA systems. In brief, processes using local shared memory share part of their address space, with threads in a process sharing their entire address space. Interprocess communication is hence provided through access to shared data. This makes it easy to express concurrency and share complex data structures, but synchronization (e.g. mutexes, atomic operations) is needed to prevent data races.

On a single computer, this can be implemented by (TODO). But is it possible to achieve the same effect on a distributed system?

\subsubsection{Distributed Shared Memory}

The goal of distributed shared memory is to allow processes on networked computers to share physical memory using a single virtual address space. The simplest possible implementation is \textbf{central server distributed shared memory}, in which all data is maintained at a server node to which all reads and writes of the data are sent to, with the server handling the requests and sending acknowledgment.
Overall, there are two main categories of DSM systems:
\begin{itemize}

  \item \textbf{Object based} DSM systems, which allow the sharing of individual objects between computers in a distributed system. This is a pure software approach, and allowing granularity to be determined by object size reduces false sharing.

  \item \textbf{Page-based} DSM systems, which can leverage paging hardware (with OS help). The unit of sharing is (a multiple of) page size, meaning false sharing is more likely.

\end{itemize}

\subsection{Page-Based Distributed Shared Memory}

In page-based DSM, physical memory on each node holds pages in the shared virtual address space. \textbf{Local pages} are those shared in the current node's memory, whereas \textbf{remote pages} are in some other node's memory. Each node also has \textbf{private} (non-shared) memory.

To implement page-table based DSM, MMU hardware must be leveraged. Pages with valid page table entries are local, with access to a non-local page triggering a page fault. The DSM protocol can then handle the page fault and retrieve the remote data, these operations being transparent to the programmer. This can be implemented at the user level using standard OS services.

One way to implement a user-level DSM system would be to maintain metadata about each shared page, similar to an OS page table entry (e.g. stating whether it is valid/invalid, read/write permission, etc.). Access can then be controlled with \verb|mmap| and/or \verb|mprotect|, and a \verb|SIGSEGV| handler can be installed by the DSM library to catch invalid access.

\subsection{The Atomic Page Update Problem}

Multiple threads in the process share the OS page table. Hence, we need to control the case of multiple threads accessing the ``missing" page. The page must be accessible to allow the DSM protocol to update it.

One solution is to map a file to \textit{two} virtual addresses: one for the application and one for the DSM system, using different protections on each. Specifically, the application mapping has \verb|PROT_NONE|, whereas the DSM mapping has \verb|PROT_READ ||\verb| PROT_WRITE|. We then make \verb|SIGSEGV| allow access to the DSM system address to update the page, and only grant access to the application address when the page fault is fully handled.

\subsection{Locating Remote Data}

The simplest possible design is again a central server. Specifically, the central server maintains a directory recording which machine currently stores each page. The page \textit{migrates} to the node where each most recent access occurs. Unfortunately, this solution presents numerous problems:

\begin{itemize}

  \item Problem 1: the directory at the central server becomes a bottleneck, as all page accesses need to go through the central server.

  Potential solution: a distributed directory, in which each node is responsible for a given portion of the address space. For example, we could have
  \[\text{responsible node} \equiv \text{page num.} \mod \text{no. nodes}\]

  \item Problem 2: each virtual page exists on only one machine, i.e. no caching. Actively shared pages may hence lead to thrashing as they are copied back and forth between users, even if no writes occurs.

  Potential solution: allow replication (caching). Read operations become cheaper, as simultaneous reads can be executed locally on each reader node. Write operations, however, become more expensive, as cached copies must be invalidated or updated.

\end{itemize}

\subsection{Replication}

\subsubsection{Simple Replication (Read Replication)}

In simple replication, we have \textbf{multiple readers, single writer} (MRSW). One node can be granted a \textit{read-write copy}, \textbf{or} multiple nodes can be granted read-only access.
On a read operation, we set access rights to read-only on any writeable copy on other nodes (of which there are at most one). We then acquire a read-only copy of the page.
On a write operation, we revoke write permissions from the other writeable copy (if any), and get a read-write copy of the page. We invalidate all copies of the page at other nodes.

\subsubsection{Full Replication}

In full replication, more than one node can have a writable copy of the page. Access to shared data must hence be controlled to maintain consistency. As we must keep track of copies of a page, the directory must be extended with a \textbf{copyset}, i.e. the set of all nodes that requested copies. On a request for a page copy, we add the requestor to the copyset and send the page contents. On a request to invalidate a page, we send invalidation requests to all nodes in the copyset and wait for acknowledgments.

\subsection{Consistency}

\begin{definition}[Consistency Model]
A \textbf{consistency model} defines
\begin{enumerate}

  \item When modifications to data may be seen at a given processor

  \item How memory will apprear to a programmer, restricting what values can be returned by a read of a memory location.

\end{enumerate}
\end{definition}
A consistency model must be well-understood, as it determines how the programmer reasons about the correctness of a program and what optimizations are allowed.

\subsubsection{Sequential Consistency}

Recall the definition of sequential consistency:
\begin{definition}[Sequential Consistency]
In the \textbf{sequential consistency} model,
\begin{enumerate}
  \item All memory operations must execute one at a time
  \item All operations of a single processor must execute in the same order as given in \textbf{program order}
  \item Interleaving among processors is OK, \textbf{but all processors must experience the same interleaving}.
\end{enumerate}
\end{definition}
To achieve sequential consistency, nodes must ensure that the previous memory operation is complete before beginning the next one. Therefore, they must get acknowledgment writes have completed. With caching, they must send invalidate or update messages to all copies. All these messages must be acknowledged. To improve performance, we relax the rules.

\subsubsection{Relaxed (Weak) Consistency}

There are several different ``flavors" of weak consistency, depending on which sequential consistency requirement we are relaxing. For example, we can relax program order, write atomicity, data races and reordering constraints. We can also allow reads/writes to different memory locations to be reordered. To see why we can do so, consider operation in a critical section. Synchronization should be used for all shared data operations, hence, only one thread is actively reading or writing. Since no other thread will access shared data until the current thread leaves the critical section, there is no need to propagate writes sequentially, or at all, until the thread leaves the critical section.

\subsubsection{Synchronization Variables}

We apply the following consistency requirements to synchronization variables:
\begin{itemize}

  \item Accesses to synchronization variables are sequentially consistent

  \item No access to a synchronization variable may be performed until all previous writes have been completed everywhere

  \item No data access may be performed until all previous accesses to synchronization variables have been performed

\end{itemize}
In essence, this is an operation for synchronizing memory, analogous to \textbf{fences} in shared memory multiprocessors: all local writes are propagated, all remote writes are brought into the local processor, and we block until memory is synchronized.

There are some problems here:
\begin{itemize}

  \item Synchronization happens at the beginning and end of a critical section. But we don't know whether the process is \textit{finished} memory access or about to \textit{start}.

  \item The system must make sure that all locally-initiated writes have completed and all remote writes have been obtained.

\end{itemize}
Can we do better?

\subsubsection{Release Consistency}

We separate synchronization into two stages:
\begin{itemize}

  \item \textbf{Acquire access:} obtain valid copies of all pages

  \item \textbf{Release access:} send invalidations for shared pages that were modified locally to nodes that have copies.

\end{itemize}
This is called \textbf{Eager Release Consistency}.
Release requires sending invalidations to all nodes with a copy \textit{and} waiting for acknowledgments. We can do better by delaying this process: on release, we send an invalidation to the \textit{directory}, and on acquire, we check with the directory to see if a new copy is needed. This is called \textbf{Lazy Release Consistency}.

\subsection{Propagating Changes}

There are two possible strategies for propagating changes to a page: we can send the entire page, which is easy but may require a lot of data, or we can send only what changed. This, however, requires that the local system save the original and compute differences.

One potential algorithm is to create a diff at Release: changes are encoded into the diff, the twin is discarded, and the page is marked invalid due to modifications at the other node. On the next access, diffs are exchanged and applied.

\subsection{Page Allocation and Replacement}

Each node has limited physical memory to cache pages for DSM. We can perform eviction to local disk or to another node. In this case, each page must still be ``owned" by some node, even if multiple copies exist. Victim selection can take page characteristics into account:
\begin{itemize}

  \item Read-only copies owned by another node can be discarded

  \item Read-only copies owned by the evicting node require at least an ownership transfer

  \item Read-write copies require actual page transfer

\end{itemize}

\section{Time, Clocks and Ordering on Distributed Systems}

In a distributed system, each machine maintains its own time, there being no global shared clock. Hence, some solution must exist to synchronize the clocks in the system. The simplest possible solution is to have a time server maintaining a global notion of time, which each machine periodically contacting the time server asking for the current global time and updating their (skewed) local time. One problem with this approach is that requests are not instantaneous. Christian's algorithm (1989) attempts to address this by
\begin{itemize}

  \item Client \(P\) requests the time from server \(S\)

  \item \(S\) responds with the time \(T\) from its own clock

  \item \(P\) sets its time to be \(T + R/2\), where \(R\) is the time the request took to process, that is, the round-trip time (RTT).

\end{itemize}
This assumes propagation delay is the same for send and receive. The accuracy can be improved by making multiple requests and then using the one with the minimum round-trip time.

We can also use the Berkeley Algorithm (1989).
\begin{enumerate}

  \item A \textbf{master} is chosen by election.

  \item The \textbf{master} polls the \textbf{slaves} who reply with their time.

  \item The \textbf{master} observes the RTT of the messages and estimates the time of each \textbf{slave}.

  \item The \textbf{master} averages the slave and own clock times, ignoring outliers.

  \item The \textbf{master} sends out the amount (positive or negative) that each \textbf{slave} must adjust its clock to get to the average.

\end{enumerate}

The most commonly used protocol is the Network Time Protocol. Time servers form a tree, layered into strata, with stratum 0 being a reliable external time source. Stratum 1 servers are directly connected to the source, and other servers are connected via network links, UDP. This gives millisecond precision in WAN and microsecond precision in LAN.

Precision time protocol: 

\section{Byzantine Generals}

\section{Distributed Agreement}

\section{Group Communication and Fault Tolerance}

\section{Zookeeper}

\section{Reliable, High Performance Storage}

\section{Soft Updates and LFS}

\section{SSDs and F2FS}

\section{Security Basics}

\printbibliography

\end{document}
